//
//  CounterViewModel.swift
//  TallyCounterWatchApp WatchKit Extension
//
//  Created by Jessica Wienadi on 25/08/21.
//

import Foundation
import CoreData

class CounterViewModel: ObservableObject {
//    var didChange = PassthroughSubjec
    
    @Published var count = 0
    @Published var start = 0
    @Published var interval = 1
    @Published var isMinimum = false
    
    func addCounter() {
        count += interval
        if count >= 0 {
        isMinimum = false
        }
    }
    
    func substractCounter() {
        count -= interval
        if count <= 0 {
        isMinimum = true
        count = 0
        }
    }
    
    func saveCount(context: NSManagedObjectContext,title: String){
        let newSavedCount = SavedCount(context: context)
        newSavedCount.countTitle = title
        newSavedCount.countNumber = Int16(count)
        newSavedCount.dateSaved = Date()
        
        do {
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
        
    }
    
}

