//
//  CounterView.swift
//  TallyCounterWatchApp WatchKit Extension
//
//  Created by Jessica Wienadi on 25/08/21.
//

import SwiftUI



struct CounterView: View {
    
    @EnvironmentObject var counter: CounterViewModel
    @State var disabled = true
    
//    init(counter: CounterViewModel) {
//        self.counter = counter
//       }
    
    var body: some View {
        VStack {
    //buton minus
            Button(action: {counter.substractCounter()}
//                    {
//                counter.count -= counter.interval
//                if counter.count <= 0 {
//                disabled = true
//                }
//            }
            ) {
                Text("-")
                    .font(.title)
                    .fontWeight(.bold)
                    .multilineTextAlignment(.center)
            }
            .buttonStyle(BorderedButtonStyle(tint: .clear))
            .foregroundColor(.white)
            .background(Color.pink)
            .cornerRadius(10)
            .padding(.horizontal)
//            .frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: .top)
            .disabled(disabled)
            
  
            
    //angka counter
            Text(String(counter.count))
                .font(.largeTitle)
                .fontWeight(.semibold)
                .padding(.vertical)
            
    //button plus
            Button(action:{counter.addCounter()
//                counter.count += counter.interval
//                print(counter.count)
                if counter.count >= 0 {
                disabled = false
                }
            }) {
                Text("+")
                    .font(.title2)
                    .fontWeight(.bold)
            }
            .buttonStyle(BorderedButtonStyle(tint: .clear))
            .foregroundColor(.white)
            .background(Color.purple)
            .cornerRadius(10)
            .padding(.horizontal)

        }
    }
}

struct CounterView_Previews: PreviewProvider {
    static var previews: some View {
        CounterView().environmentObject(CounterViewModel())
    }
}
