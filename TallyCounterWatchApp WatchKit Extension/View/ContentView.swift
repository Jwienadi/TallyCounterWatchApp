//
//  ContentView.swift
//  TallyCounterWatchApp WatchKit Extension
//
//  Created by Jessica Wienadi on 25/08/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        
        
        List {
                
            HStack{
                VStack(alignment: .leading){
                    Text("Jemaat Siang")
                        .font(.caption)
                        .fontWeight(.medium)
                
                    Text(Date(), style: .date)
                        .font(.footnote)
                        .foregroundColor(.secondary)
                }
                
                Spacer()
                    
                Text("101")
                    .font(.title2)
                    .fontWeight(.thin)
//                    .frame(alignment: .trailing)
                }
          
            
                
//                Text(save.countNumber ?? "NN")
//                    .font(.body)
                
        }
        .listRowPlatterColor(.clear)

        }
    }


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
