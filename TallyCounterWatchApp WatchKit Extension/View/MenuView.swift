//
//  MenuView.swift
//  TallyCounterWatchApp WatchKit Extension
//
//  Created by Jessica Wienadi on 25/08/21.
//

import SwiftUI


struct MenuView: View {
    
    @EnvironmentObject var counter: CounterViewModel
    @State var selection: Int? = nil
    
    
    //coba
    @State private var isShowingSave = false
    @State private var isShowingInterval = false
    
    var body: some View {
        
            VStack(spacing: 10) {
                HStack(spacing: 0){
                    MenuButton(icon: "gobackward", title: "Reset", color: Color.purple){
                        counter.count = 0
                        print(counter.count)
                    }
                    
                    Spacer()
                    
        //modal
                    MenuButton(icon: "square.and.arrow.down", title: "Save", color: Color.blue){
                        isShowingSave.toggle()
                    }
                    .sheet(isPresented: $isShowingSave, content: {
                        SaveCountAs()
                    })
                
                }
                //tutupnya hstack atas
                   

                HStack(spacing: 0){
//                    MenuButton(icon: "archivebox", title: "Saved List", color: Color.red){
//                        print("a")
//                    }
                    NavigationLink(destination: EmptyView()) {
                        EmptyView()
                    }
                    .buttonStyle(PlainButtonStyle())
                    
                    VStack{
                    
                                        NavigationLink(
                                            destination: SavedView(), tag: 1, selection: $selection){
                    
                                            Button(action: {
                                                self.selection = 1
                                            }){
                                                Image(systemName: "archivebox")
                                            }
                    
                                        }
                                        .buttonStyle(BorderedButtonStyle(tint: Color.blue))
                                        .buttonStyle(PlainButtonStyle())
                    
                                        Text("Saved List")
                                            .font(.footnote)
                                    }
                    Spacer()
                    
                    VStack{
                    
                                        NavigationLink(
                                            destination: SetIntervalView().environmentObject(counter), tag: 2, selection: $selection){
                    
                                            Button(action: {
                                                self.selection = 2
                                            }){
                                                Image(systemName: "square.and.pencil")
                                            }
                    
                                        }
                                        .buttonStyle(BorderedButtonStyle(tint: Color.purple))
                                        .buttonStyle(PlainButtonStyle())
                    
                                        Text("Set Interval")
                                            .font(.footnote)
                                    }
//                    NavigationLink(destination: EmptyView()) {
//                        EmptyView()
//                    }
                    
                    
                    
//                    MenuButton(icon: "square.and.pencil", title: "Set interval", color: Color.red){
//                            isShowingInterval.toggle()
//                        }
//                        .sheet(isPresented: $isShowingInterval, content: {
//                            SetIntervalView()
//                        })
//
//
                }
            }
            .navigationTitle("Menu")
        }
    }



struct MenuButton: View {
    var icon: String
    var title: String

    var color: Color
    var clicked: (() -> Void)
    
    var body: some View {
        
        VStack{
            Button(action: {
                clicked()
            }){
                Image(systemName: icon)
            }
            .buttonStyle(BorderedButtonStyle(tint: color))
            
            Text(title)
                .font(.footnote)
                
        }
    }
}



struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuView().environmentObject(CounterViewModel())
    }
}
