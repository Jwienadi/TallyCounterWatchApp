//
//  SavedView.swift
//  TallyCounterWatchApp WatchKit Extension
//
//  Created by Jessica Wienadi on 25/08/21.
//

import SwiftUI


struct SavedView: View {
        
    @Environment(\.managedObjectContext) private var context
    
    @FetchRequest(sortDescriptors: [])
    var saves:FetchedResults<SavedCount>
    
    
    var body: some View {
        
        List {
            ForEach(saves, id:\.self) {save in
                
            HStack{
                VStack(alignment: .leading){
                    Text(save.countTitle ?? "Unitled")
                        .font(.caption)
                        .fontWeight(.medium)
                
                    Text(save.dateSaved!, style: .date)
                        .font(.footnote)
                        .foregroundColor(.secondary)
                }
                    
                Spacer()
                
                Text("\(save.countNumber)")
                    .font(.title2)
                    .fontWeight(.thin)
                }
            
            
                
//                Text(save.countNumber ?? "NN")
//                    .font(.body)
                
                
            }
            .onDelete(perform: countDelete)

        }
        .overlay(Text(saves.isEmpty ? "No Saved Count":""))
        .navigationTitle("Saved")
    }
    
    private func countDelete(at offsets: IndexSet){
        for offset in offsets{
            let save = saves[offset]
            context.delete(save)
            try? self.context.save()
        }
    }
    
}


struct SavedView_Previews: PreviewProvider {
    static var previews: some View {
        SavedView().environmentObject(CounterViewModel())
    }
}


    

