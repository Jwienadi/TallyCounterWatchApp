//
//  SetIntervalView.swift
//  TallyCounterWatchApp WatchKit Extension
//
//  Created by Jessica Wienadi on 26/08/21.
//

import SwiftUI

struct SetIntervalView: View {
    @EnvironmentObject var counter: CounterViewModel
    @State private var intervalNum: Int = 0
  
    
    var body: some View {
        
        VStack{
            Text("Current Interval: \(counter.interval)")
                .font(.caption2)
            
            Spacer()

            Picker(selection: $intervalNum, label: Text("Set Number"), content: {

                ForEach(1..<11) { i in
                    Text("\(i)").tag(i)
                }
            })
            .labelsHidden()
            .defaultWheelPickerItemHeight(25.0)
            .frame(height: 50)
            
            Spacer()

                
            
            Button(action:{
                    counter.interval = intervalNum + 1
                print("savebtn")
                
            }
            ){
                Text("Save")
            }
//            .frame(height:30)
         
            
            Button(action:{
                    counter.interval = 1
                print("savebtn")
                
            }
            ){
                Text("Reset")
            }
        
        }
    }
}

struct SetIntervalView_Previews: PreviewProvider {
    static var previews: some View {
        SetIntervalView()
    }
}
