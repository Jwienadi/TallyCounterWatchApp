//
//  SaveCountAs.swift
//  TallyCounterWatchApp WatchKit Extension
//
//  Created by Jessica Wienadi on 25/08/21.
//


import SwiftUI

struct SaveCountAs: View {
    @Environment(\.managedObjectContext) private var context
    @Environment(\.presentationMode) var presentationMode
    
   @State private var countTitleText: String = ""
    @EnvironmentObject var counter: CounterViewModel
    
    
    
    var body: some View {
        
        VStack{
        
            TextField("Count Title...", text: $countTitleText)
                .padding()
            
            Button(action:
                    {
        counter.saveCount(context: context, title: countTitleText)
                        
        self.presentationMode.wrappedValue.dismiss()
                        
                    }
            ){
                Text("Save")
            }
            .padding()
            
        
        }
//        .navigationTitle("Save As")
    }
}

struct SaveCountAs_Previews: PreviewProvider {
    static var previews: some View {
        SaveCountAs().environmentObject(CounterViewModel())
    }
}

