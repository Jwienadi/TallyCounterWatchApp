//
//  PersistentController.swift
//  TallyCounterWatchApp WatchKit Extension
//
//  Created by Jessica Wienadi on 25/08/21.
//

import Foundation
import CoreData

struct PersistentController {
    
    static let shared = PersistentController()
    
    let container: NSPersistentContainer
    
    init(inMemory: Bool = false) {
        container = NSPersistentContainer(name: "SavedCount")
        if inMemory {
            container.persistentStoreDescriptions.first!.url = URL(fileURLWithPath: "/dev/null")
        }
        
        container.loadPersistentStores {(storeDesc, error) in
            if let error = error as NSError? {
                fatalError("Failed to load container \(error)")
            }
            
        }
    }
    
}
