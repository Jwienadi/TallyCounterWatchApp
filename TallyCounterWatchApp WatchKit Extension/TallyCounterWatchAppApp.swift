//
//  TallyCounterWatchAppApp.swift
//  TallyCounterWatchApp WatchKit Extension
//
//  Created by Jessica Wienadi on 25/08/21.
//

import SwiftUI

@main
struct TallyCounterWatchAppApp: App {
    
    private let container = PersistentController.shared.container
    @StateObject var counter = CounterViewModel()
    @State var tabSelection = 2
    
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView{
                TabView(selection: $tabSelection)  {
                    MenuView().environmentObject(counter)
                        .tag(1)
                    CounterView().environmentObject(counter)
                        .tag(2)
//                    SavedView().environmentObject(counter)
//                        .tag(3)
                }
    //            .environmentObject(CounterViewModel)
                .tabViewStyle(PageTabViewStyle())
                
                
            }
            .environment(\.managedObjectContext, container.viewContext)
        }

//        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
